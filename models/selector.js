import moment from 'moment';
import { compose } from 'redux';
import { createSelector } from 'reselect';
import { EXCHANGE_SEARCH_STATUS } from '../../constants';
import {
	byHash,
	filter,
	getByFrom,
	identity,
	map,
	Maybe,
	maybe,
	prop,
	sort,
} from '../../../../../common/utils/functional';
import { routeSelector } from '../../selectors/helper';

const getRoot = state => state.exchangeSearch;

export const momentDepartureComparer = (a, b) =>
	moment.parseZone(a.departureDate).diff(moment.parseZone(b.departureDate));

export const segmentsMapper = datesComparer => (segments, route) => {
	const getSegmentById = getByFrom('id')(segments);

	const getSortedSegments = compose(
		filter(identity),
		sort(datesComparer),
		map(getSegmentById),
		prop('segments'),
	);

	return maybe([])(getSortedSegments)(Maybe.of(route));
};

const segmentsSelector = datesComparer => (segments, routes) => {
	if (!routes || !routes.length) {
		return [];
	}
	return routes.reduce(
		(acc, route) => [
			...acc,
			...segmentsMapper(datesComparer)(segments, route),
		],
		[]
	);
};

export const getExchangeStatus = createSelector(
	getRoot,
	state => state.status || EXCHANGE_SEARCH_STATUS.NONE
);

export const getIsExchangeSearchFailed = createSelector(
	getExchangeStatus,
	status => status === EXCHANGE_SEARCH_STATUS.UNAVAILABLE
);

export const getIsExchangeSearchEmpty = createSelector(
	getExchangeStatus,
	status => status === EXCHANGE_SEARCH_STATUS.EMPTY
);

export const getDidOffersNotReceived = createSelector(
	[getIsExchangeSearchEmpty, getIsExchangeSearchFailed],
	(isEmpty, isFailed) => isEmpty || isFailed
);

// ACTUAL RESULT

const withUpsalePrice = offer => {
	if (!offer || !offer.upsales || !offer.upsales.length) {
		return offer;
	}

	const upsalePrices = offer.upsales.reduce(
		(acc, { price, alias, description }) => {
			if (price && price.value > 0) {
				return {
					...acc,
					[alias]: {
						value: price.value,
						label: description,
					},
				};
			}
			return acc;
		},
		{}
	);

	return {
		...offer,
		price: {
			...offer.price,
			...upsalePrices,
		},
	};
};

export const getActualResult = createSelector(
	getRoot,
	state => state.actualResult
);

export const getOffers = createSelector(
	getActualResult,
	state => state.offers.map(withUpsalePrice)
);

const getNewSegments = createSelector(
	getActualResult,
	state => state.segments
);

const getNewRoutes = createSelector(
	getActualResult,
	state => state.routes
);


export const getOffer = hash => createSelector(
	[getOffers],
	offers =>
		getByFrom('offerHash')(offers)(hash)
);

export const getOfferByRouteId = (routeId: string) => createSelector(
	[getOffers],
	offers => offers.find(offer => offer.routes.indexOf(routeId) > -1)
);

export const getExchangeOffers = createSelector(
	[getExchangeStatus, getOffers],
	(status, offers) => {
		if (status === EXCHANGE_SEARCH_STATUS.FOUND) {
			return offers;
		}
		return [];
	}
);
export const getNewRoute = routeId => createSelector(
	getNewRoutes,
	routes => routes.find(byHash(routeId))
);


export const createGetNewSegmentsOrdered = (routeHash, datesComparer = momentDepartureComparer) =>
	createSelector(
		[getNewSegments, getNewRoute(routeHash)],
		segmentsMapper(datesComparer)
	);

export const getRoutesForOffer = hash => createSelector(
	[getOffer(hash), getNewRoutes],
	(offer, routes) => {
		if (!offer) {
			return;
		}

		return offer.routes.map(routeHash => routes.find(byHash(routeHash)));
	}
);

export const createGetNewSegmentsForOffer = (hash, datesComparer = momentDepartureComparer) =>
	createSelector(
		[getNewSegments, getRoutesForOffer(hash)],
		segmentsSelector(datesComparer)
	);

export const getNewRouteForExistingRoute = (offerHash, routeId) => createSelector(
	getRoutesForOffer(offerHash),
	newRoutes => newRoutes.find(r => r.existingRouteId === routeId)
);

// CACHED RESULTS


// getCachedOffer :: ([a], Object) -> Maybe(a)
export const getOfferedCache = createSelector(
	[getRoot],
	state => state.offered
);
export const getCachedSegments = createSelector(
	[getOfferedCache],
	state => state.segments
);
export const getCachedRoutes = createSelector(
	[getOfferedCache],
	state => state.routes
);
export const getCachedOffers = createSelector(
	[getOfferedCache],
	state => state.offers.map(withUpsalePrice)
);
export const getCachedOffer = hash => createSelector(
	[getCachedOffers],
	offers => {
		const find = a => a.find(o => o.hash === hash);

		return Maybe.of(offers).map(find);
	}
);

export const getCachedRoutesForOffer = offerHash => createSelector(
	[getCachedRoutes, getCachedOffer(offerHash)],
	(cachedRoutes, offer) => {
		if (offer.isNothing) {
			return [];
		}

		return offer.$value.routes.map(hash => routeSelector({ routeId: hash })(cachedRoutes));
	}
);

export const getCachedRouteByHash = hash => createSelector(
	[getCachedRoutes],
	cachedRoutes =>
		routeSelector({ routeId: hash })(
			cachedRoutes
		)
);

export const createGetCachedSegmentsForOfferOrdered =
	(hash, datesComparer = momentDepartureComparer) =>
		createSelector(
			[getCachedSegments, getCachedRoutesForOffer(hash)],
			segmentsSelector(datesComparer)
		);

export const createGetCachedSegmentsForRouteOrdered =
	(hash, datesComparer = momentDepartureComparer) =>
		createSelector(
			[getCachedSegments, getCachedRouteByHash(hash)],
			segmentsMapper(datesComparer)
		);

export const getFailedOffersFetchCount = createSelector(
	[getRoot],
	state => state.failedOffersFetchCount
);

export const getCachedRouteForExistingRoute = (offerHash, routeId) => createSelector(
	getCachedRoutesForOffer(offerHash),
	newRoutes => newRoutes.find(r => r.existingRouteId === routeId)
);

