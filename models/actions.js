import { EXCHANGE_SEARCH_STATUS } from '../../constants';
import type { Offer } from './types';
import type { Route, Segment } from '../order/types';

export const ON_SEARCH_STATUS_CHANGED = 'ON_EXCHANGE_SEARCH_STATUS_CHANGED';
export const onSearchStatusChanged = (status: $Keys<typeof EXCHANGE_SEARCH_STATUS>, routeId: any = '', note) => ({
	type: ON_SEARCH_STATUS_CHANGED,
	payload: { status, routeId, note },
});

type LogParams = {
	selectedReason: string,
}

export const ON_SEARCH_FAIL = 'ON_SEARCH_FAIL';
export const onSearchFailed = (status: string, params: LogParams, error: any) => ({
	type: ON_SEARCH_FAIL,
	payload: { status, params, error },
});

type Result = {
	airports: Array<{}>,
	carriers: Array<{}>,
	cities: Array<{}>,

	offers: Array<{}>,
	offeredRoutes: Array<{}>,
	offeredSegments: Array<{}>,
}


export const ON_SEARCH_SUCCESS = 'ON_SEARCH_SUCCESS';
export const onSearchSuccess = (result: Result, logParams: LogParams) => {
	const {
		offeredSegments,
		offeredRoutes,
	} = result;

	return {
		type: ON_SEARCH_SUCCESS,
		payload: {
			offers: result.offers,
			routes: offeredRoutes,
			segments: offeredSegments,

			logParams,
		},
	};
};

export const ON_OFFERS_CLEAN = 'ON_OFFERS_CLEAN';
export const onOffersClean = () => ({
	type: ON_OFFERS_CLEAN,
});

export const ON_SELECT_OFFER = 'ON_SELECT_OFFER';
export const onSelectOffer = hash => ({
	type: ON_SELECT_OFFER,
	payload: { hash },
});

export const ON_OFFERS_REQUEST = 'ON_OFFERS_REQUEST';
export const onOffersRequest = () => ({
	type: ON_OFFERS_REQUEST,
});
export const ON_SKIP_OFFERS_CLICK = 'ON_SKIP_OFFERS_CLICK';
export const onSkipOffersClick = ({ isOnLoading }: { isOnLoading: boolean }) => ({
	type: ON_SKIP_OFFERS_CLICK,
	payload: { isOnLoading },
});
export const ON_CONFIRM_OFFERS_CLICK = 'ON_CONFIRM_OFFERS_CLICK';
export const onConfirmOffersClick = (isTicketSelected: boolean, refund: boolean) => ({
	type: ON_CONFIRM_OFFERS_CLICK,
	payload: { isTicketSelected, refund },
});

export const ON_RESTORE_OFFERS = 'ON_RESTORE_OFFERS';
export const onRestoreOffers = (offers: Offer[], routes: Route[], segments: Segment[]) => ({
	type: ON_RESTORE_OFFERS,
	payload: {
		offers,
		routes,
		segments,
	},
});

export const ON_ADD_OFFERED_ROUTE_DETAILS = 'ON_ADD_OFFERED_ROUTE_DETAILS';
export const onAddOfferedRouteDetails = (offer: {}, routes: Array<{}>, segments: Array<{}>) => ({
	type: ON_ADD_OFFERED_ROUTE_DETAILS,
	payload: {
		offer,
		routes,
		segments,
	},
});
