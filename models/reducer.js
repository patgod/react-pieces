import {
	ON_OFFERS_CLEAN, ON_RESTORE_OFFERS,
	ON_SEARCH_FAIL,
	ON_SEARCH_STATUS_CHANGED,
	ON_SEARCH_SUCCESS,
	ON_ADD_OFFERED_ROUTE_DETAILS,
} from './actions';
import type { Action } from '../types';
import { mergeArrayOfObjects } from '../../helpers/array-merge';
import { EXCHANGE_SEARCH_STATUS } from '../../constants';

type State = {
	routeId: string,
	routes: Array<{}>,
	segments: Array<{}>,
	status: $Keys<typeof EXCHANGE_SEARCH_STATUS>,
	areOffersSkipped: boolean,

	actualResult: {
		offers: Array<{}>,
		routes: Array<{}>,
		segments: Array<{}>,
	},

	offered: {
		offers: Array<{}>,
		routes: Array<{}>,
		segments: Array<{}>,
	},
	failedOffersFetchCount: number,
};

export const defaultState = {
	routeId: '',
	routes: [],
	segments: [],
	status: EXCHANGE_SEARCH_STATUS.NONE,
	areOffersSkipped: false,

	actualResult: {
		offers: [],
		routes: [],
		segments: [],
	},

	offered: {
		offers: [],
		routes: [],
		segments: [],
	},
	failedOffersFetchCount: 0,
};

export default (state: State = defaultState, action: Action): State => {
	switch (action.type) {
		case ON_SEARCH_STATUS_CHANGED: {
			const { status } = action.payload;

			return Object.assign({}, state, {
				status,
			});
		}
		case ON_SEARCH_SUCCESS: {
			const { offers, routes, segments } = action.payload;

			return Object.assign({}, state, {
				status: EXCHANGE_SEARCH_STATUS.FOUND,
				actualResult: {
					offers,
					routes,
					segments,
				},
			});
		}
		case ON_OFFERS_CLEAN: {
			return Object.assign({}, state, {
				status: EXCHANGE_SEARCH_STATUS.NONE,
				actualResult: defaultState.actualResult,
			});
		}
		case ON_RESTORE_OFFERS: {
			const { offers, routes, segments } = action.payload;
			return {
				...state,
				offered: {
					offers,
					routes,
					segments,
				},
			};
		}
		case ON_ADD_OFFERED_ROUTE_DETAILS: {
			const { offer, routes, segments } = action.payload;

			return Object.assign({}, state, {
				offered: {
					offers: mergeArrayOfObjects(state.offered.offers, [offer], { key: 'hash' }),
					routes: mergeArrayOfObjects(state.offered.routes, routes, { key: 'hash' }),
					segments: mergeArrayOfObjects(state.offered.segments, segments, { key: 'hash' }),
				},
			});
		}
		// Ловим его тут, чтобы сохранить между перезагрузками
		case ON_SEARCH_FAIL: {
			return {
				...state,
				failedOffersFetchCount: state.failedOffersFetchCount + 1,
			};
		}
		default:
			return state;
	}
};
