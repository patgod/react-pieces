import {
	getCachedOffer,
	getNewRoute,
	getOfferByRouteId,
	getFailedOffersFetchCount,
	getRoutesForOffer,
	createGetNewSegmentsForOffer,
	createGetCachedSegmentsForOfferOrdered,
	createGetCachedSegmentsForRouteOrdered,
} from '../selector';
import { getCacheForSelectedOffer } from '../../crossSelector';
import dataOffers from '../../../../../helpers/__mocks__/exchange-offers.ow.response.json';
import fakeState from './fake.state.json';
import { Maybe } from '../../../../../../common/utils/functional';

describe('New Offer', () => {
	const state = {
		exchangeSearch: {
			actualResult: {
				offers: dataOffers.offers,
				routes: dataOffers.offeredRoutes,
			},
		},
	};

	it('должен получить routeId по хешу предложения', () => {
		const result = getRoutesForOffer('offer_1')(state);

		expect(result).toMatchSnapshot();
	});
	it('should вернуть ничего', () => {
		const result = getRoutesForOffer('offer_2')(state);

		expect(result).toBe(undefined);
	});

	it('should достать офер по routeId', () => {
		const result = getOfferByRouteId('87618484')(state);

		expect(result).toBe(dataOffers.offers[1]);
	});
});

describe('New Route', () => {
	const state = { exchangeSearch: fakeState };
	it('должен получить route по хешу предложения', () => {
		const result = getNewRoute('87618437')(state);

		expect(result).toMatchSnapshot();
	});
	it('should вернуть ничего', () => {
		const result = getNewRoute('wrong')(state);

		expect(result).toBe(undefined);
	});
});

describe('OfferedCache', () => {
	const state = {
		claimForm: {
			claimItems: [
				{ passengerId: 'p2', routeId: 'r20' },
				{ passengerId: 'p1', routeId: 'r10' },
			],
			offerHash: 'offer_1',
		},
		exchangeSearch: {
			offered: {
				offers: [
					{
						hash: 'offer_1',
						routes: ['r10'],
						price: {
							fee: 100,
							total: 200,
						},
						upsales: [
							{ alias: 'insurance', description: 'She bop', price: { value: -10 } },
							{ alias: 'aeroexpress', description: 'Hey baba rebop', price: { value: 30 } },
							{ alias: 'baggage', description: '2 bears beat', price: { value: 40 } },
						],
					},
				],
				routes: [
					{ hash: 'r10', segments: ['s1', 's3'] },
					{ hash: 'r2', segments: ['s2'] },
				],
				segments: [
					{ hash: 's1' },
					{ hash: 's2' },
					{ hash: 's3' },
				],
			},
		},
	};

	it('должен вернуть cachedOffer', () => {
		const result = getCachedOffer('offer_1')(state);

		expect(result).toEqual(new Maybe({
			hash: 'offer_1',
			routes: ['r10'],
			price: {
				fee: 100,
				baggage: { value: 40, label: '2 bears beat' },
				aeroexpress: { value: 30, label: 'Hey baba rebop' },
				total: 200,
			},
			upsales: [
				{ alias: 'insurance', description: 'She bop', price: { value: -10 } },
				{ alias: 'aeroexpress', description: 'Hey baba rebop', price: { value: 30 } },
				{ alias: 'baggage', description: '2 bears beat', price: { value: 40 } },
			],
		}));
	});

	it('должен получить сегменты и рут по OFFER_HASH', () => {
		const result = getCacheForSelectedOffer(state);

		expect(result).toEqual({
			offer: {
				hash: 'offer_1',
				routes: ['r10'],
				price: {
					fee: 100,
					baggage: { value: 40, label: '2 bears beat' },
					aeroexpress: { value: 30, label: 'Hey baba rebop' },
					total: 200,
				},
				upsales: [
					{ alias: 'insurance', description: 'She bop', price: { value: -10 } },
					{ alias: 'aeroexpress', description: 'Hey baba rebop', price: { value: 30 } },
					{ alias: 'baggage', description: '2 bears beat', price: { value: 40 } },
				],
			},
			routes: [{ hash: 'r10', segments: ['s1', 's3'] }],
			segments: [
				{ hash: 's1' },
				{ hash: 's3' },
			],
		});
	});
});


describe('Негативное извлечение', () => {
	const state = {
		claimForm: {
			claimItems: [
				{ passengerId: 'p2', routeId: 'r20' },
				{ passengerId: 'p1', routeId: 'r10' },
			],
			offerHash: 'offer_2',
		},
		exchangeSearch: {
			offered: {
				offers: [
				],
				routes: [
				],
				segments: [
				],
			},
		},
	};

	it('должен вернуть undefined для cachedOffer', () => {
		const result = getCachedOffer('dich')(state);

		expect(result).toEqual(Maybe.of(undefined));
	});

	it('должен вернуть undefined и не выбросить исключений', () => {
		const result = getCacheForSelectedOffer(state, { submitId: { passengerId: 'p1', routeId: 'r10' } });

		expect(result).toEqual({
			offer: undefined,
			routes: [],
			segments: [],
		});
	});
});

describe('SearhcFailCount', () => {
	const state = {
		exchangeSearch: {
			failedOffersFetchCount: 34,
		},
	};

	it('Выбран url для exchange', () => {
		expect(getFailedOffersFetchCount(state)).toEqual(34);
	});
});


describe('Получение рутов для оффера', () => {
	const state = {
		exchangeSearch: {
			actualResult: {
				offers: [
					{ hash: 'o1', routes: ['r1', 'r3'] },
					{ hash: 'o2', routes: ['r2'] },
				],
				routes: [
					{ hash: 'r1' },
					{ hash: 'r2' },
					{ hash: 'r3' },
				],
			},
		},
	};

	it('Выбраны руты оффера', () => {
		expect(getRoutesForOffer('o1')(state)).toEqual([
			{ hash: 'r1' },
			{ hash: 'r3' },
		]);
	});
});


describe('Получение сегментов для оффера', () => {
	const state = {
		exchangeSearch: {
			actualResult: {
				offers: [
					{ hash: 'o1', routes: ['r1', 'r3'] },
					{ hash: 'o2', routes: ['r2'] },
				],
				routes: [
					{ hash: 'r1', segments: ['s1', 's2'] },
					{ hash: 'r2', segments: ['s3', 's4'] },
					{ hash: 'r3', segments: ['s5', 's6'] },
				],
				segments: [
					{ hash: 's1' },
					{ hash: 's2' },
					{ hash: 's3' },
					{ hash: 's4' },
					{ hash: 's5' },
					{ hash: 's6' },
					{ hash: 's7' },
				],
			},
		},
	};

	it('Выбраны руты оффера', () => {
		expect(createGetNewSegmentsForOffer('o1')(state)).toEqual([
			{ hash: 's1' },
			{ hash: 's2' },
			{ hash: 's5' },
			{ hash: 's6' },
		]);
	});
});

describe('Получение кешированных сегментов', () => {
	const state = {
		exchangeSearch: {
			offered: {
				offers: [
					{ hash: 'o1', routes: ['r1', 'r3'] },
					{ hash: 'o2', routes: ['r2'] },
				],
				routes: [
					{ hash: 'r1', segments: ['s1', 's2'] },
					{ hash: 'r2', segments: ['s3', 's4'] },
					{ hash: 'r3', segments: ['s5', 's6'] },
				],
				segments: [
					{ hash: 's1' },
					{ hash: 's2' },
					{ hash: 's3' },
					{ hash: 's4' },
					{ hash: 's5' },
					{ hash: 's6' },
					{ hash: 's7' },
				],
			},
		},
	};

	describe('для оффера с несколькими рутами', () => {
		const result = createGetCachedSegmentsForOfferOrdered('o1')(state);

		it('Должны выбраться нужные сегменты', () => {
			expect(result).toEqual([
				{ hash: 's1' },
				{ hash: 's2' },
				{ hash: 's5' },
				{ hash: 's6' },
			]);
		});
	});

	describe('для оффера', () => {
		const result = createGetCachedSegmentsForOfferOrdered('o2')(state);

		it('Должны выбраться нужные сегменты', () => {
			expect(result).toEqual([
				{ hash: 's3' },
				{ hash: 's4' },
			]);
		});
	});

	describe('для рута', () => {
		const result = createGetCachedSegmentsForRouteOrdered('r3')(state);

		it('Должны выбраться нужные сегменты', () => {
			expect(result).toEqual([
				{ hash: 's5' },
				{ hash: 's6' },
			]);
		});
	});
});
