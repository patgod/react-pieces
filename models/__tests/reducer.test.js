import {
	onSearchStatusChanged,
	onSearchSuccess,
	onOffersClean,
	onRestoreOffers,
	onAddOfferedRouteDetails,
	onSearchFailed,
} from '../actions';
import reducer, { defaultState } from '../reducer';
import { EXCHANGE_SEARCH_STATUS } from '../../../constants';

describe('Action default', () => {
	const result = reducer(undefined, { type: 'other' });

	it('Получаем дефолтный стейт', () => {
		expect(result).toEqual(defaultState);
	});
});

describe('Action onSearchStatusChanged', () => {
	const action = onSearchStatusChanged('new_status');

	const result = reducer(undefined, action);

	it('В стейте должны сброситься флаги', () => {
		expect(result).toEqual({
			...defaultState,
			status: 'new_status',
		});
	});
});

describe('Action onSearchSuccess', () => {
	it('Без связи связи маршрутов и офферов, routes должны быть сохранены', () => {
		const action = onSearchSuccess({
			airports: [],
			carriers: [],
			cities: [],

			offers: [{ id: 'o1' }],
			offeredRoutes: [{ id: 'r1' }, { id: 'r2' }, { id: 'r3' }],
			offeredSegments: [{ id: 's1' }],
		});

		const result = reducer(undefined, action);

		expect(result).toEqual({
			...defaultState,
			status: EXCHANGE_SEARCH_STATUS.FOUND,
			actualResult: {
				offers: [{ id: 'o1' }],
				routes: [{ id: 'r1' }, { id: 'r2' }, { id: 'r3' }],
				segments: [{ id: 's1' }],
			},
		});
	});

	it('Взяты все руты', () => {
		const action = onSearchSuccess({
			airports: [],
			carriers: [],
			cities: [],

			offers: [{ id: 'o1', routes: ['r1'] }, { id: 'o2', routes: ['r3', 'r4'] }],
			offeredRoutes: [{ hash: 'r1' }, { hash: 'r2' }, { hash: 'r3' }, { hash: 'r4' }],
			offeredSegments: [{ id: 's1' }, { id: 's2' }],
		});

		const result = reducer(undefined, action);

		expect(result).toEqual({
			...defaultState,
			status: EXCHANGE_SEARCH_STATUS.FOUND,
			actualResult: {
				offers: [{ id: 'o1', routes: ['r1'] }, { id: 'o2', routes: ['r3', 'r4'] }],
				routes: [{ hash: 'r1' }, { hash: 'r2' }, { hash: 'r3' }, { hash: 'r4' }],
				segments: [{ id: 's1' }, { id: 's2' }],
			},
		});
	});
});

describe('Action onOffersClean', () => {
	const action = onOffersClean();

	const result = reducer({
		...defaultState,
		status: EXCHANGE_SEARCH_STATUS.FOUND,
		actualResult: {
			offers: [{ id: 'o1', routes: ['r1'] }, { id: 'o2', routes: ['r3', 'r4'] }],
			routes: [{ hash: 'r1' }, { hash: 'r3' }],
			segments: [{ id: 's1' }, { id: 's2' }],
		},
	}, action);

	it('В стейте должны сброситься флаги', () => {
		expect(result).toEqual({
			...defaultState,
		});
	});
});


describe('Action onRestoreOffers', () => {
	const action = onRestoreOffers([1, 2], [3, 4], [5, 6]);

	const result = reducer(undefined, action);

	it('В стейте должны сброситься флаги', () => {
		expect(result).toEqual({
			...defaultState,
			offered: {
				offers: [1, 2],
				routes: [3, 4],
				segments: [5, 6],
			},
		});
	});
});


describe('Action onAddOfferedRouteDetails', () => {
	const action = onAddOfferedRouteDetails({ hash: 'o2' }, [{ hash: 'r2' }], [{ hash: 's2' }, { hash: 's3' }]);

	const result = reducer({
		...defaultState,
		offered: {
			offers: [{ hash: 'o1' }],
			routes: [{ hash: 'r1' }, { hash: 'r2' }],
			segments: [{ hash: 's2' }],
		},
	}, action);

	it('В стейте должны сброситься флаги', () => {
		expect(result).toEqual({
			...defaultState,
			offered: {
				offers: [{ hash: 'o1' }, { hash: 'o2' }],
				routes: [{ hash: 'r1' }, { hash: 'r2' }],
				segments: [{ hash: 's2' }, { hash: 's3' }],
			},
		});
	});
});

describe('Action onAddOfferedRouteDetails', () => {
	const action = onSearchFailed();

	const result = reducer({
		...defaultState,
		failedOffersFetchCount: 2,
	}, action);

	it('В стейте должны сброситься флаги', () => {
		expect(result).toEqual({
			...defaultState,
			failedOffersFetchCount: 3,
		});
	});
});
