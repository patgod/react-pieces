import moment from 'moment';
import { all, call, put, select } from 'redux-saga/effects';
import { validateTheForm } from './submit';
import { _window as windowEffects } from './effects';
import api from '../../../helpers/api';

import {
	filterClaimItem,
	getClaimItem,
	getClaimItems,
	getSelectedReason,
	hasClaimItem,
} from '../models/claimForm/selector';

import { onBeforeMoveToRefundFromExchange } from '../models/crossActions';
import {
	onAddOfferedRouteDetails,
	onOffersClean,
	onSearchFailed,
	onSearchStatusChanged,
	onSearchSuccess,
} from '../models/actions';
import { getPathForRouteIndex } from '../routing';
import { EXCHANGE_SEARCH_STATUS, FORM_TYPE, momentFormat } from '../constants';
import {
	getOffer,
	getRoutesForOffer,
	createGetNewSegmentsForOffer,
} from '../models/selector';
import { getRefundUrl } from '../models/url/selector';
import { saveForm } from './storage';
import {
	getIsExchangeSearchAvailable, getParamsForRoute,
	getParamsForSchedules,
	getPayloadForOffersRequest,
} from '../models/crossSelector';
import { ROUTE } from '../routing/constants';
import { onDictionariesLoaded } from '../models/dictionary/actions';
import { loadRoutingConfig } from './routing';
import { onFormBusy, onItemsUpdate, onReasonSet, onUpdateForm } from '../models/claimForm/actions';
import { getVoidInfo } from '../models/order/selector';
import { onScheduleLoaded } from '../models/schedule/actions';
import { getRawSchedule, getSchedules } from '../models/schedule/selector';

export const onAnyChange = context => function* (action) {
	yield saveForm(context, action);

	if (context.isSubmitted()) {
		yield validateTheForm(context);
	}
};


export function* onChangeField (action) {
	const { name, value } = action.payload;
	yield put(onUpdateForm({ [name]: value }));
}

export const moveToRefund = context => function* () {
	const refundUrl = yield select(getRefundUrl);
	const voidInfo = yield select(getVoidInfo);


	if (voidInfo.isAvailable) {
		yield put(onBeforeMoveToRefundFromExchange({ isVoidAvailable: true }));

		const history = context.getHistory();

		const pathForRoute = getPathForRouteIndex(ROUTE.AUTOVOID.FORM);

		history.push(pathForRoute);
		window.scrollTo(0, 0);
	} else {
		yield put(onFormBusy());
		yield put(onBeforeMoveToRefundFromExchange({ isVoidAvailable: false }));
		yield call(windowEffects.redirect, refundUrl);
	}
};

export function* onSelectOffer (action) {
	const { hash } = action.payload;

	const newOffer = yield select(getOffer(hash));
	const newRoutes = yield select(getRoutesForOffer(hash));
	const newSegments = yield select(createGetNewSegmentsForOffer(hash));

	yield put(onAddOfferedRouteDetails(newOffer, newRoutes, newSegments));

	yield put(onOffersClean());
}

function* resetExchangeSearchStatus (context) {
	if (context.getFormType() === FORM_TYPE.exchange) {
		yield put(onSearchStatusChanged(EXCHANGE_SEARCH_STATUS.NONE));

		yield loadRoutingConfig(context);
	}
}

export const onCheckRoute = context => function* (action) {
	const { submitId } = action.payload;
	const {
		routeId,
	} = submitId;

	let newItems;

	const hasItem = yield select(hasClaimItem(routeId));

	if (hasItem) {
		newItems = yield select(filterClaimItem(routeId));
	} else {
		newItems = yield select(getClaimItems);
		newItems.push({
			routeId,
		});
	}

	yield put(onItemsUpdate(newItems));

	yield resetExchangeSearchStatus(context);
};

export const onSetDate = context => function* (action) {
	const { submitId, date } = action.payload;

	yield setExchangeDate(context, submitId, date);
};

export function* setExchangeDate (context, submitId, date) {
	const {
		routeId,
	} = submitId;

	let item;
	let newItems;

	const hasItem = yield select(hasClaimItem(routeId));

	if (hasItem) {
		item = yield select(getClaimItem(routeId));
		newItems = yield select(filterClaimItem(routeId));
	} else {
		item = {
			routeId,
		};
		newItems = yield select(getClaimItems);
	}

	newItems.push(Object.assign({}, item, {
		newDepartureDate: date,
	}));

	yield put(onItemsUpdate(newItems));

	yield resetExchangeSearchStatus(context);
}

export function* setOffersFlag () {
	const items = yield select(getClaimItems);

	const newItems = items.map(item => ({
		...item,
		withOffers: true,
	}));

	yield put(onItemsUpdate(newItems));
}

export const fetchOffers = context => function* () {
	const isExchangeSearchAvailable = yield select(getIsExchangeSearchAvailable);

	const selectedReason = yield select(getSelectedReason);

	const params = yield select(getPayloadForOffersRequest);

	const logParams = {
		...params,
		selectedReason,
	};

	if (selectedReason && isExchangeSearchAvailable) {
		yield put(onSearchStatusChanged(EXCHANGE_SEARCH_STATUS.INITIALIZED, params));

		yield setOffersFlag();

		try {
			const response = yield api.getOffers(
				context.getOrderId(),
				params
			);

			if (response.offers.length) {
				yield put(onDictionariesLoaded(response));

				yield put(onSearchSuccess(response, logParams));

				dumpDebugInfo(response);
			} else {
				yield onOffersSearchFail(
					context,
					EXCHANGE_SEARCH_STATUS.EMPTY,
					{
						...logParams,
						searchTypes: response.searchTypes,
					},
					'response_without_offers'
				);
			}
		} catch (e) {
			yield onOffersSearchFail(context, EXCHANGE_SEARCH_STATUS.UNAVAILABLE, logParams, e);
		}
	} else {
		yield put(onSearchStatusChanged(EXCHANGE_SEARCH_STATUS.NONE, logParams, 'flag_in_the_data'));
	}
};

function* onOffersSearchFail (context, status, params, e) {
	yield put(onSearchStatusChanged(status, params, e));
	yield put(onSearchFailed(status, params, e));

	yield loadRoutingConfig(context);

	const _history = context.getHistory();

	_history.push(getPathForRouteIndex(ROUTE.FORM.ROUTES));
}

function dumpDebugInfo (response) {
	try {
		const debugArray = response.offers.map(o => o.debug).filter(_ => _);

		if (debugArray.length) {
			// eslint-disable-next-line
			console.log(
				'Offers debug: ',
				debugArray
			);
		}
	// eslint-disable-next-line
	} catch (e) {}
}

export const onCheckReason = context => function* (action) {
	const { id } = action.payload;

	yield put(onReasonSet(id));

	yield loadRoutingConfig(context);
};

export const onRoutesStepLoaded = context => function* () {
	if (context.getFormType() === FORM_TYPE.refund) {
		return;
	}
	const existingSchedules = yield select(getSchedules);
	const filter = ({ id }) => {
		if (!existingSchedules.length) {
			return true;
		}
		return !existingSchedules.find(
			schedule => schedule.routeId === id
		);
	};
	const paramsByRoutes = yield select(getParamsForSchedules(filter));

	try {
		const promises = paramsByRoutes.map(api.getExchangeSchedule);

		const responses = yield all(promises);

		for (let i = 0; i < responses.length; i += 1) {
			const {
				routeId,
				dateFrom,
				dateTo,
			} = paramsByRoutes[i];

			yield put(onScheduleLoaded({
				routeId,

				dateFrom,
				dateTo,

				items: responses[i] || [],
			}));
		}
	} catch (e) {
		if (typeof Raven !== 'undefined') {
			Raven.captureMessage('Не удалось получить расписания для exchange', {
				level: 'warning',
				extra: { routes: paramsByRoutes, e },
			});
		}
	}
};

export const onExchangeDatePickerMonthChange = function* (action) {
	const { routeId, date } = action.payload;

	const currentSchedule = yield select(getRawSchedule(routeId));

	if (!currentSchedule) {
		return;
	}

	let theMoment = moment(date);

	const toTheLeft = theMoment.isSameOrBefore(currentSchedule.dateFrom, 'month');
	const toTheRight = theMoment.isSameOrAfter(currentSchedule.dateTo, 'month');

	if (currentSchedule && currentSchedule.items.length && (toTheLeft || toTheRight)) {
		let newDateFrom;
		let newDateTo;

		if (toTheLeft) {
			theMoment.subtract(3, 'month');
			theMoment.startOf('month');

			const now = moment();

			if (theMoment.isBefore(now)) {
				theMoment = now;
			}

			newDateFrom = theMoment.format(momentFormat.apiDate);
			newDateTo = currentSchedule.dateFrom;
		} else {
			newDateFrom = currentSchedule.dateTo;
			theMoment.add(3, 'month');
			theMoment.endOf('month');
			newDateTo = theMoment.format(momentFormat.apiDate);
		}

		const params = yield select(getParamsForRoute(routeId));

		params.dateFrom = newDateFrom;
		params.dateTo = newDateTo;

		try {
			const response = yield api.getExchangeSchedule(params);

			const {
				dateFrom,
				dateTo,
			} = params;

			yield put(onScheduleLoaded({
				routeId,

				dateFrom,
				dateTo,

				items: response || [],
			}));
		} catch (e) {
			if (typeof Raven !== 'undefined') {
				Raven.captureMessage('Не удалось получить дополнительное расписания для exchange', {
					level: 'warning',
					extra: { routes: params, e },
				});
			} else {
				console.warn('Не удалось получить дополнительное расписания для exchange');
			}
		}
	}
};

