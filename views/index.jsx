// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { testAttribute } from '@tutu/autotest';
import getLabel from '@tutu/get-label';
import withIsDesktop from '@tutu-hoc/is-desktop';

import ImpossibleRoute from '../../../../components/ImpossibleRoute';
import OffersList from '../../../../components/Offers';
import StepSubmit from '../../../../components/ButtonSubmit';
import { getRoute } from '../../../../../routing';
import * as exchangeSearchActions from '../../../../../models/exchangeSearch/actions';
import { getExchangeOffers, getExchangeStatus } from '../../../../../models/exchangeSearch/selector';
import { EXCHANGE_SEARCH_STATUS } from '../../../../../constants';
import { ROUTE } from '../../../../../routing/constants';
import { getScreensValidity } from '../../../../../models/claimForm/selector';
import type { Offer } from '../../../../../models/exchangeSearch/types';
import ButtonToRoutes from '../../../../components/ButtonToOffers';

import labels from '../../../../../routing/labels/rus/routing.json';
import * as claimFormActions from '../../../../../models/claimForm/actions';
import * as crossActions from '../../../../../models/crossActions';
import { getHasChanges } from '../../../../../models/order/selector';

import styles from './styles.less';

type OuterProps = {
	validationClass: string,
	onLoad: () => mixed,
	type: string
}

type InnerProps = {
	status: string,
	offers: Offer[],
	routeIndex: string,
	isMobile: boolean,
	hasChanges: boolean,

	validation: {},
	screensValidity: {},

	onSelectOffer: (string) => mixed,
	onSkipOffersClick: (boolean) => mixed,
	onConfirmOffersClick: (boolean, boolean) => mixed,
	onSubmitStep: ({}) => mixed,
	onOffersRequest: () => mixed,
	onClearSelectedOffer: () => mixed,
	onMoveToRefundFromExchange: () => mixed,
	onForceOffersSkip: () => mixed,
}

export type PropTypes = OuterProps & InnerProps;

type State = {
	skip: boolean,
	refund: boolean,
	offerHash: string,
}

class Offers extends Component<PropTypes, State> {
	constructor (props: PropTypes) {
		super(props);

		const { onOffersRequest } = props;

		onOffersRequest();

		this.state = {
			skip: false,
			refund: false,
			offerHash: '',
		};
	}

	componentDidMount () {
		const { onLoad } = this.props;

		onLoad();
	}

	onConfirm = () => {
		const { onConfirmOffersClick } = this.props;
		const { skip, offerHash, refund } = this.state;
		this.onApply();
		onConfirmOffersClick(!!offerHash && !skip, refund);
	};

	onSkip = () => {
		const { onSkipOffersClick } = this.props;

		onSkipOffersClick(true);

		this.onApply(true);
	};

	onChooseOffer = (hash: string) => {
		this.setState(
			{
				offerHash: hash,
				skip: false,
			},
			this.onApply
		);
	};

	onApply = (forceSkip: boolean = false) => {
		const { onSelectOffer, onClearSelectedOffer, onMoveToRefundFromExchange } = this.props;
		const { skip, refund, offerHash } = this.state;


		if (!refund && !skip && !forceSkip && offerHash) {
			onSelectOffer(offerHash);
		} else if (refund) {
			onMoveToRefundFromExchange();
			return;
		} else {
			onClearSelectedOffer();
		}

		this.onSubmitStep();
	};

	onChooseSkipOption = () => {
		const { onSkipOffersClick } = this.props;
		const { skip } = this.state;

		onSkipOffersClick(false);


		this.setState(
			{
				skip: !skip,
				offerHash: '',
				refund: false,
			},
			this.onApply
		);
	};

	onChooseRefundOption = () => {
		const { refund } = this.state;
		this.setState(
			{
				refund: !refund,
				offerHash: '',
				skip: false,
			},
			this.onApply
		);
	};

	onSubmitStep = () => {
		const { routeIndex, onSubmitStep } = this.props;
		const theRoute = getRoute(routeIndex);
		const { onSubmit: createAction } = theRoute.submit;
		onSubmitStep(
			createAction(theRoute.stepNumber)
		);
	};

	render () {
		const {
			routeIndex,
			offers,
			status,
			screensValidity,
			isMobile,
			hasChanges,
		} = this.props;

		const theRoute = getRoute(routeIndex);

		if (!screensValidity[theRoute.previousIndex]) {
			return (
				<ImpossibleRoute />
			);
		}

		const { offerHash, skip, refund } = this.state;

		const isConfirmEnabled = status === EXCHANGE_SEARCH_STATUS.FOUND
			&& (!!offerHash || skip || refund);

		let submitTitle = '';

		if (skip) {
			submitTitle = getLabel(labels, 'FORM_ROUTES.submit.exchange.default', {});
		}

		if (refund) {
			submitTitle = getLabel(labels, 'FORM_ROUTES.submit.refund.default', {});
		}

		return (
			<div {...testAttribute('offers')}>

				<OffersList
					offerHash={offerHash}
					isSkipped={skip}
					isRefundSelected={refund}
					onChooseOffer={this.onChooseOffer}
					onChooseSkipOption={this.onChooseSkipOption}
					onChooseRefundOption={this.onChooseRefundOption}
					onSkip={this.onSkip}
					status={status}
					offers={offers}
					routeIndex={routeIndex}
					isMobile={isMobile}
					hasChanges={hasChanges}
				/>

				<StepSubmit
					onSubmitStep={this.onConfirm}
					routeIndex={routeIndex}
					isValid={isConfirmEnabled}
					isVisible
					isRefund={false}
					title={submitTitle}
					childrenDisplay="flex"
					isPinned
					showPrimary={false}
				>
					<ButtonToRoutes
						responsify={false}
						routeIndex={routeIndex}
						className={isMobile ? styles.backButton : ''}
					/>
				</StepSubmit>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	status: getExchangeStatus(state),
	offers: getExchangeOffers(state),
	routeIndex: ROUTE.FORM.OFFERS,
	validation: state.claimForm.validation.offers,
	screensValidity: getScreensValidity(state),
	hasChanges: getHasChanges(state),
});

const mapDispatchToProps = dispatch => ({
	onSelectOffer: hash => dispatch(exchangeSearchActions.onSelectOffer(hash)),
	onSkipOffersClick: isOnLoading => dispatch(
		exchangeSearchActions.onSkipOffersClick({ isOnLoading })
	),
	onConfirmOffersClick: (isTicketSelected, refund) => dispatch(
		exchangeSearchActions.onConfirmOffersClick(isTicketSelected, refund)
	),
	onSubmitStep: action => {
		dispatch(action);
	},
	onOffersRequest: () => {
		dispatch(exchangeSearchActions.onOffersRequest());
	},
	onClearSelectedOffer: () => {
		dispatch(claimFormActions.onClearSelectedOffer());
	},
	onMoveToRefundFromExchange: () => {
		dispatch(crossActions.onMoveToRefundFromExchange());
	},
});

export const OffersDisconnected = Offers;

export default compose(
	withIsDesktop,
	connect(mapStateToProps, mapDispatchToProps)
)(Offers);
